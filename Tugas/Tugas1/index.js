import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator} from "@react-navigation/stack";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";

import { SignIn, Profile, Home, Details, Details2, Details3, Details4 } from './Screens';

const AuthStack = createStackNavigator();
const Tabs = createBottomTabNavigator();
const HomeStack = createStackNavigator();
const ProfileStack = createStackNavigator();

const HomeStackScreen = () => (
    <HomeStack.Navigator>
        <HomeStack.Screen name="Home" component={Home} />
        <HomeStack.Screen name="Details" component={Details} options={({route})=>({
            title: route.params.name
        })} />
        <HomeStack.Screen name="Details2" component={Details2} options={({route})=>({
            title: route.params.name
        })} />
        <HomeStack.Screen name="Details3" component={Details3} options={({route})=>({
            title: route.params.name
        })} />
        <HomeStack.Screen name="Details4" component={Details4} options={({route})=>({
            title: route.params.name
        })} />
    </HomeStack.Navigator>
)

const ProfileStackScreen = () => (
    <ProfileStack.Navigator>
        <ProfileStack.Screen name="Profile" component={Profile} />
    </ProfileStack.Navigator>
)

export default () => (
    <NavigationContainer>
        <AuthStack.Navigator>
            <AuthStack.Screen name="SignIn" component={SignIn} options={{title:'Sign In' }} />
            <AuthStack.Screen name="Home" component={Home} options={{title:'Home' }} />
        </AuthStack.Navigator>
        <Tabs.Screen name="Home" component={HomeStackScreen} />
        <Tabs.Screen name="Profile" component={ProfileStackScreen} />
    </NavigationContainer>
);