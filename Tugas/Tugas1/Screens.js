import React from "react";
import {View, Text, StyleSheet, Button} from "react-native";

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "space-around",
        alignItems: 'center',
      },
      title: {
        backgroundColor: 'whitesmoke',
        color: '#4A90E2',
        fontSize: 50,
        padding: 10,
      },
      header: {
        backgroundColor: 'whitesmoke',
        color: '#4A90E2',
        fontSize: 40,
        padding: 10,
      },
      body: {
        backgroundColor: 'whitesmoke',
        color: '#4A90E2',
        fontSize: 24,
        padding: 10,
      },
      boxSmall: {
        width: 200,
        height: 200,
        marginBottom: 10,
        marginRight: 10,
        backgroundColor: 'skyblue',
      },
      box: {
        width: 250,
        height: 250,
        marginBottom: 10,
        marginRight: 10,
        backgroundColor: 'skyblue',
      },
      boxLarge: {
        width: 300,
        height: 300,
        marginBottom: 10,
        marginRight: 10,
        backgroundColor: 'steelblue',
      },
      image: {
        width: 200,
        height: 200,
      },
})




const ScreenContainer = ({ children }) => (
    <View style={styles.container}>{children}</View>
);

export const Home = ({navigation}) => (
    <ScreenContainer>
        <ScrollView style={styles.container}>
        <View style={styles.boxLarge} />
        <Text style={styles.title}>Hello!</Text>
        <Text style={styles.header}>What's your occasion?</Text>
        <ScrollView horizontal>
          <View style={styles.boxSmall} />
          <Image source={require('./components/download.jpg')} style={{width:98}}/>
          <Button title="Wedding" onPress={() => navigation.push("Details", {name: 'Wedding' })} />
          <View style={styles.boxSmall} />
          <Image source={require('./components/birthday.jpg')} style={{width:98}}/>
          <Button title="Birthday" onPress={() => navigation.push('Details2', {name: 'Birthday' })} />
          <View style={styles.boxSmall} />
          <Image source={require('./components/graduation.jpg')} style={{width:98}}/>
          <Button title="Graduation" onPress={() => navigation.push('Details3', {name: 'Graduation' })} />
          <View style={styles.boxSmall} />
          <Image source={require('./components/funeral.jpg')} style={{width:98}}/>
          <Button title="Funeral" onPress={() => navigation.push('Details4', {name: 'Funeral' })} />
          <View style={styles.boxSmall} />
        </ScrollView>
        <View style={styles.boxLarge} />
        <View style={styles.boxSmall} />
        <View style={styles.boxLarge} />
      </ScrollView>
    </ScreenContainer>
);

export const Details = () => (
    <ScreenContainer>
        <View style={styles.container}>
          <Image source={require('./components/download.jpg')} style={{width:98}}/>
          <Text style={styles.title}>Wedding Bouquet</Text>
          <Text style={styles.body}>Description: </Text>
          <Text style={styles.body}>- 7 roses strands</Text>
          <Text style={styles.body}>- Flower strands available in white, pink, soft blue, cream, or mix.</Text>
          <Text style={styles.body}>- High quality</Text>
          <Text style={styles.body}>- High quality</Text>
          <Text style={styles.body}>Price: </Text>
          <Text style={styles.body}>Rp 100.000</Text>
      </View>
    </ScreenContainer>
);


export const Details2 = () => (
    <ScreenContainer>
        <View style={styles.container}>
          <Image source={require('./components/birthday.jpg')} style={{width:98}}/>
          <Text style={styles.title}>Birthday Bouquet</Text>
          <Text style={styles.body}>Description: </Text>
          <Text style={styles.body}>- 7 roses strands</Text>
          <Text style={styles.body}>- Flower strands available in white, pink, soft blue, cream, or mix.</Text>
          <Text style={styles.body}>- High quality</Text>
          <Text style={styles.body}>- High quality</Text>
          <Text style={styles.body}>Price: </Text>
          <Text style={styles.body}>Rp 100.000</Text>
      </View>
    </ScreenContainer>
);

export const Details3 = () => (
    <ScreenContainer>
        <View style={styles.container}>
          <Image source={require('./components/graduation.jpg')} style={{width:98}}/>
          <Text style={styles.title}>Graduation Bouquet</Text>
          <Text style={styles.body}>Description: </Text>
          <Text style={styles.body}>- 7 roses strands</Text>
          <Text style={styles.body}>- Flower strands available in white, pink, soft blue, cream, or mix.</Text>
          <Text style={styles.body}>- High quality</Text>
          <Text style={styles.body}>- High quality</Text>
          <Text style={styles.body}>Price: </Text>
          <Text style={styles.body}>Rp 100.000</Text>
      </View>
    </ScreenContainer>
);

export const Details4 = () => (
    <ScreenContainer>
        <View style={styles.container}>
          <Image source={require('./components/funeral.jpg')} style={{width:98}}/>
          <Text style={styles.title}>Funeral Bouquet</Text>
          <Text style={styles.body}>Description: </Text>
          <Text style={styles.body}>- 7 roses strands</Text>
          <Text style={styles.body}>- Flower strands available in white, pink, soft blue, cream, or mix.</Text>
          <Text style={styles.body}>- High quality</Text>
          <Text style={styles.body}>- High quality</Text>
          <Text style={styles.body}>Price: </Text>
          <Text style={styles.body}>Rp 100.000</Text>
      </View>
    </ScreenContainer>
);

export const SignIn = ({navigation}) => (
    <ScreenContainer>
        <View style={styles.container}>
        <Text>username</Text>
        <TextInput
          style={{ height: 40, borderColor: 'gray', borderWidth: 1 }}
          onChangeText={(text) => this.setState({ value: text })}
          value={this.state.value}
        />
        <Text>password</Text>
        <TextInput
          style={{ height: 40, borderColor: 'gray', borderWidth: 1 }}
          onChangeText={(text) => this.setState({ value1: text })}
          value={this.state.value1}
        />
        <Button title="Submit" onPress={() => navigation.push('Home', {name: 'Submit' })} />
      </View>
    </ScreenContainer>
);

